<?php
  require_once 'src/Poneys.php';

  class PoneysTest extends \PHPUnit_Framework_TestCase {



    public function test_removePoneyFromField() {
      // Setup
      $Poneys = new Poneys();

      // Action
      $Poneys->removePoneyFromField(3);
      
      // Assert
      $this->assertEquals(5, $Poneys->getCount());

      //Negatif
}

  public function setUp(){

  $Poneys = new Poneys();
  $Poneys->setCount(18) ;


  }


    public function tearDown(){
      
        unset($this->Poneys);



    }



    /**
     * @dataProvider NegatifProvider
     */

    public function testNegatif($a)

    {
       // Setup
       $Poneys = new Poneys();
       $Poneys->removePoneyFromField($a);
       

    }
    public function NegatifProvider()
    {
        return [
            [2],
            [1],
            [4],
            [3]
        ];
    }



    /**
     * @dataProvider NegatifProviderResult
     */

    public function testNegatifwithResult($a,$expected)

    {
       // Setup
        $Poneys = new Poneys();
        $Poneys->removePoneyFromField($a);
        $this->assertEquals($expected,$Poneys->getCount());
       
    }
    public function NegatifProviderResult()
    {
        return [
            [2,6],
            [1,7],
            [0,8],
            [3,5]
        ];
    }

public function test_getNames() {

  $Poneys = new Poneys();
  $Poneys = $this->getMockBuilder('Poneys')->getMock() ; 
  $Poneys->expects($this->exactly(1))
         ->method('getNames')
         ->willReturn(array('nawal','mounia','safae','hanaa')
          );

         $this->assertEquals(
          array('nawal','mounia','safae','hanaa'),
          $Poneys->getNames()

          );


        
}



 public function testDisponibilite()

    {
       // Setup
        $Poneys = new Poneys();
        //$Poneys->addPoneyToField(15);
       $this->assertTrue($Poneys->placeDisponible());

    }


  }
 ?>
